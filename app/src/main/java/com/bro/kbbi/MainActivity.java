package com.bro.kbbi;

import android.app.DatePickerDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.Calendar;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void process(View view){
        EditText inpTinggi = findViewById(R.id.inpTinggi);
        EditText inpTahun = findViewById(R.id.inpTahunLahir);
        EditText inpNama = findViewById(R.id.inpNama);
        RadioButton rbLaki = findViewById(R.id.rbLaki);
        RadioButton rbWani = findViewById(R.id.rbWani);
        Spinner spinner = findViewById(R.id.inpAct);

        //menghitung menghitung bbi
        Integer tb = Integer.parseInt(inpTinggi.getText().toString());
        Float bbi = (tb-100) * 0.9f;

        //menghitung kkb
        Float kkb = 0f;
        if(rbLaki.isChecked()){
            kkb = 30 * bbi;
        } else if(rbWani.isChecked()){
            kkb = 25 * bbi;
        } else{
            kkb = 0f;
        }

        //menghitung presentase aktifitas
        Float activity = 0f;
        switch (spinner.getSelectedItemPosition()){
            case 0:
                activity = 0.4f;
                break;
            case 1:
                activity = 0.2f;
                break;
            case 2:
                activity = 0.1f;
                break;
        }

        //menghitung faktor koreksi
        Integer umur = 2019 - Integer.parseInt(inpTahun.getText().toString());
        Float fk = 0f;
        if(umur <= 39 && umur > 0 ){
            fk = 0f;
        } else if(umur <= 59 && umur >= 40){
            fk = 0.05f;
        } else if(umur <= 69 && umur >= 60){
            fk = 0.1f;
        } else if(umur > 70){
            fk = 0.2f;
        }

        //menghitung nilai KKT
        Float kkt = 0f;
        kkt = kkb + ((kkb * activity) - (kkb * fk));

        inpNama.setText("KKT " + kkt);
    }

    public void showTanggal(View view){
        Calendar c = Calendar.getInstance();
        DatePickerDialog date = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                EditText inpTahun = findViewById(R.id.inpTahunLahir);
                inpTahun.setText(year + "");
            }
        },c.get(Calendar.YEAR),c.get(Calendar.MONTH), c.get(Calendar.DAY_OF_MONTH));
        date.show();
    }

}
